﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cars
{
    class Car
    {
        public string Title;
        public double Fuel;
        public double RateFuel;

        public string GO()
        {
            if (Fuel<RateFuel)
            {
                return "В автомобиле '" + Title + "' закончился запас топлива";
            }
            else
            {
                Fuel -= RateFuel;
                return "В автомобиле '" + Title + "' осталось " + Fuel + " единиц топлива";
            }
        }

        public bool CanMove()
        {
            bool a;
            if (Fuel < RateFuel)
            {
                a = false;
            }
            else
            {
                a = true;
            }

            return a;
        }
    }
}
