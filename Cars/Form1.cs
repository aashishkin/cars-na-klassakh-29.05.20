﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cars
{
    public partial class FormMain : Form
    {
        Car[] ArrCar = new Car[10];
        int index = 0;
            
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonAddCar_Click(object sender, EventArgs e)
        {
            if (index<=9)
            {
                ArrCar[index] = new Car()
                {
                    Title = textBoxTitle.Text,
                    Fuel = double.Parse(textBoxMaxFuel.Text),
                    RateFuel = double.Parse(textBoxRateFuel.Text)
                };
                index++;

                textBoxMaxFuel.Clear();
                textBoxRateFuel.Clear();
                textBoxTitle.Clear();

                labelCarsCount.Text = index + "/10 машин";
            }
            else
            {
                MessageBox.Show("Слишком много автомобилей", "Ошибка!");
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            textBoxGo.Clear();
            int temp = 0;
            bool oneCanMove = true;
            int count = 0;
            
            
            while (oneCanMove)
            {
                count++;
                textBoxGo.Text += count + "км. \r\n";
                oneCanMove = false;

                for (int i = 0; i < index; i++)
                {
                    textBoxGo.Text += ArrCar[i].GO() + "\r\n";

                    if (ArrCar[i].CanMove())
                    {
                        oneCanMove = true;
                    }
                }  
            }
        }
    }
}
