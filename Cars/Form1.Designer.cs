﻿namespace Cars
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxMaxFuel = new System.Windows.Forms.TextBox();
            this.textBoxRateFuel = new System.Windows.Forms.TextBox();
            this.textBoxGo = new System.Windows.Forms.TextBox();
            this.buttonAddCar = new System.Windows.Forms.Button();
            this.buttonGo = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelMaxFuel = new System.Windows.Forms.Label();
            this.labelRateFuel = new System.Windows.Forms.Label();
            this.labelCarsCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(12, 30);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(117, 20);
            this.textBoxTitle.TabIndex = 0;
            // 
            // textBoxMaxFuel
            // 
            this.textBoxMaxFuel.Location = new System.Drawing.Point(144, 30);
            this.textBoxMaxFuel.Name = "textBoxMaxFuel";
            this.textBoxMaxFuel.Size = new System.Drawing.Size(100, 20);
            this.textBoxMaxFuel.TabIndex = 1;
            // 
            // textBoxRateFuel
            // 
            this.textBoxRateFuel.Location = new System.Drawing.Point(259, 30);
            this.textBoxRateFuel.Name = "textBoxRateFuel";
            this.textBoxRateFuel.Size = new System.Drawing.Size(119, 20);
            this.textBoxRateFuel.TabIndex = 2;
            // 
            // textBoxGo
            // 
            this.textBoxGo.Location = new System.Drawing.Point(12, 85);
            this.textBoxGo.Multiline = true;
            this.textBoxGo.Name = "textBoxGo";
            this.textBoxGo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxGo.Size = new System.Drawing.Size(366, 215);
            this.textBoxGo.TabIndex = 3;
            // 
            // buttonAddCar
            // 
            this.buttonAddCar.Location = new System.Drawing.Point(12, 56);
            this.buttonAddCar.Name = "buttonAddCar";
            this.buttonAddCar.Size = new System.Drawing.Size(366, 23);
            this.buttonAddCar.TabIndex = 4;
            this.buttonAddCar.Text = "Добавить машину";
            this.buttonAddCar.UseVisualStyleBackColor = true;
            this.buttonAddCar.Click += new System.EventHandler(this.buttonAddCar_Click);
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(12, 306);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(366, 23);
            this.buttonGo.TabIndex = 5;
            this.buttonGo.Text = "Начать заезд";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(12, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(102, 13);
            this.labelTitle.TabIndex = 6;
            this.labelTitle.Text = "Название машины";
            // 
            // labelMaxFuel
            // 
            this.labelMaxFuel.AutoSize = true;
            this.labelMaxFuel.Location = new System.Drawing.Point(141, 9);
            this.labelMaxFuel.Name = "labelMaxFuel";
            this.labelMaxFuel.Size = new System.Drawing.Size(88, 13);
            this.labelMaxFuel.TabIndex = 7;
            this.labelMaxFuel.Text = "Размер бака, л.";
            // 
            // labelRateFuel
            // 
            this.labelRateFuel.AutoSize = true;
            this.labelRateFuel.Location = new System.Drawing.Point(256, 9);
            this.labelRateFuel.Name = "labelRateFuel";
            this.labelRateFuel.Size = new System.Drawing.Size(122, 13);
            this.labelRateFuel.TabIndex = 8;
            this.labelRateFuel.Text = "Расход топлива на км.";
            // 
            // labelCarsCount
            // 
            this.labelCarsCount.AutoSize = true;
            this.labelCarsCount.Location = new System.Drawing.Point(384, 61);
            this.labelCarsCount.Name = "labelCarsCount";
            this.labelCarsCount.Size = new System.Drawing.Size(67, 13);
            this.labelCarsCount.TabIndex = 9;
            this.labelCarsCount.Text = "0/10 машин";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 339);
            this.Controls.Add(this.labelCarsCount);
            this.Controls.Add(this.labelRateFuel);
            this.Controls.Add(this.labelMaxFuel);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.buttonAddCar);
            this.Controls.Add(this.textBoxGo);
            this.Controls.Add(this.textBoxRateFuel);
            this.Controls.Add(this.textBoxMaxFuel);
            this.Controls.Add(this.textBoxTitle);
            this.Name = "FormMain";
            this.Text = "Test drive";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxMaxFuel;
        private System.Windows.Forms.TextBox textBoxRateFuel;
        private System.Windows.Forms.TextBox textBoxGo;
        private System.Windows.Forms.Button buttonAddCar;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelMaxFuel;
        private System.Windows.Forms.Label labelRateFuel;
        private System.Windows.Forms.Label labelCarsCount;
    }
}

